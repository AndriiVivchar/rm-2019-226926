import random
import math
import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
from matplotlib.path import Path
from matplotlib.patches import PathPatch

def print_matrix_by_row(matrix):
	n = np.size(matrix, 0)
	for i in range(0, n):
		format_list = ''
		for j in range(0, n):
			if type(matrix[i][j]) == str:
				format_list += matrix[i][j]
			else:
				format_list += '{0:g} '.format(matrix[i][j])
			if (j + 1) % 7 == 0:
				format_list += '\n'
		print('({0}:x):\n{1}'.format(i-max_axe, format_list))

def print_matrix_by_element(matrix):
	n = np.size(matrix, 0)
	for i in range(0, n):
		for j in range(0, n):
			if type(matrix[i][j]) == list:
				format_list = ''
				for el in matrix[i][j]:
					if type(el) == str:
						format_list += el
					else:
						format_list += '{0:g} '.format(el)
					format_list += ' '
				print('({0}:{1}): {2}'.format(i-max_axe, j-max_axe, format_list))
			else:
				if type(matrix[i][j]) == str:
					print('({0}:{1}): {2}'.format(i-max_axe, j-max_axe, matrix[i][j]))
				else:
					print('({0}:{1}): {2:.4f}'.format(i-max_axe, j-max_axe, matrix[i][j]))

def draw_plot():
	x = y = np.arange(min_axe, max_axe, 1)
	X, Y = np.meshgrid(x, y)
	Z = np.exp(-X**0)

	fig = plt.figure(figsize=(10, 10))
	ax = fig.add_subplot(111)
	ax.set_title('Metoda potencjalow')
	plt.imshow(Z, cmap=cm.RdYlGn, origin='lower', 
		   extent=[min_axe, max_axe, min_axe, max_axe])

	# draw start and finish points
	plt.plot(start_point[0], start_point[1], "or", color='red')
	plt.plot(finish_point[0], finish_point[1], "or", color='red')

	# draw obstacles
	for obstacle in obst_vect:
	    plt.plot(obstacle[0], obstacle[1], "or", color='black')

	plt.colorbar(orientation='vertical')
	plt.grid(True)
	plt.show()

def intersect_obst(point):
	for obst in obst_vect:
		if point[0] == obst[0] and point[1] == obst[1]:
			return True
	return False

def distance(point1, point2):
	return math.sqrt( pow(point2[0]-point1[0], 2) + pow(point2[1]-point1[1], 2) )

def traction(current_point):
	return kp * distance(current_point, finish_point)

def repulsive(current_point, obstacle_point):
	_distance = distance(current_point, obstacle_point) 
	if distance == 0:
		raise Exception('Can''t calculate force for points in same positions!')
	if _distance > do:
		return 0
	#else:
	return -ko * ( (1 / _distance) - (1 / do) ) * (1 / pow(_distance, 2))

def sum_repulsive(current_point):
	sum = 0;
	for obst in obst_vect:
		sum += repulsive(current_point, obst)
	return sum

def calculate_matrix():
	n = abs(min_axe) + abs(max_axe) + 1
	matrix = np.zeros( (n, n) )

	for i in range(0, n):
		for j in range(0, n):
			current_point = (i-max_axe, j-max_axe)
			if intersect_obst(current_point):
				matrix[i][j] = 'inf' 
				continue
			matrix[i][j] = traction(current_point) + sum_repulsive(current_point)

	return matrix

def force_vector(current_point):
	vector = [traction(current_point)]
	for obst in obst_vect:
		vector.append(repulsive(current_point, obst))
	return vector

def calculate_matrix_vectors():
	n = abs(min_axe) + abs(max_axe) + 1
	matrix = np.zeros( (n, n), dtype=object )

	for i in range(0, n):
		for j in range(0, n):
			current_point = (i-max_axe, j-max_axe)
			if intersect_obst(current_point):
				matrix[i][j] = 'inf' 
				continue
			matrix[i][j] = force_vector(current_point)

	return matrix

def comparator(el1, el2):
	return el1[0] - el2[0]

min_axe = -10
max_axe = 10
print('Plot x=({0},{1}), y=({0},{1})'.format(min_axe, max_axe))
start_point=(min_axe, random.randint(min_axe, max_axe))
finish_point=(max_axe, random.randint(min_axe, max_axe))
obst_vect = [
(random.randint(min_axe, max_axe), random.randint(min_axe, max_axe)), 
(random.randint(min_axe, max_axe), random.randint(min_axe, max_axe)), 
(random.randint(min_axe, max_axe), random.randint(min_axe, max_axe)), 
(random.randint(min_axe, max_axe), random.randint(min_axe, max_axe))
]
obst_vect.sort(comparator)
print('#############################################################################')
print('Obstacles vector: {0}'.format(obst_vect))
print('Final point: {0}'.format(finish_point))

kp = 1
ko = 1
do = 20
np.set_printoptions(precision=4)

print('#############################################################################')
print('Matrix of forces in each point of plot')
print('#############################################################################')
force_matrix = calculate_matrix()
#print(force_matrix)
print_matrix_by_row(force_matrix)

print('#############################################################################')
print('Matrix of vectors of forces in each point of plot: Fp, Foi')
print('#############################################################################')
force_matrix_vectors = calculate_matrix_vectors()
print_matrix_by_element(force_matrix_vectors)

draw_plot()

